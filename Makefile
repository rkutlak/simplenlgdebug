
USER=roman
SHELL=/bin/bash
# Note that everyone will have a different path.
#  Regardless of that, it should contain path to the python command and PyQt.
export PATH := /Library/Frameworks/Python.framework/Versions/3.3/bin:$(PATH)

# name of the package
package = simplenlgdebug
app     = SimpleNlgDebug.app

OPTIONS =  --resources resources/log.config.yaml,resources/images/yes.png,resources/images/no.png,resources/simplenlg.jar,resources/application.settings

# default target - run the app from place
debug: structure
	cd tmp && \
	python3 setup.py py2app -A $(OPTIONS)

release: structure
	cd tmp && \
	python3 setup.py py2app $(OPTIONS) && \
	cd dist/$(app)/Contents/ && \
	cp -R /Developer/Qt/5.3/clang_64/plugins/ PlugIns
	macdeployqt tmp/dist/$(app)

# autogenerate basic setup file -- prefer the one in main package
setup: structure
	cd tmp && \
	py2applet --make-setup $(package)/main.py

# create a new copy of the package
structure: clean
	mkdir tmp
	cp -R $(package) tmp/$(package)
	cp -R resources tmp/resources
	cp -R sassynlg/nlg tmp/$(package)/
	cp sassynlg/nlg/resources/simplenlg.jar tmp/resources/
	cp setup.py tmp/

run:
	open tmp/dist/$(app)

all: release

.PHONY : clean
clean:
	rm -rf tmp


################################################################################
