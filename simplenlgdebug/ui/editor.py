# Based on
# rowinggolfer.blogspot.co.uk/2010/08/qtextedit-with-autocompletion-using.html

import logging
logging.getLogger(__name__).addHandler(logging.NullHandler())

def get_log():
    return logging.getLogger(__name__)

    
from PyQt5 import QtWidgets, QtCore
from PyQt5.QtGui import QColor, QTextCursor
from PyQt5.QtWidgets import QTextEdit


# Logic symbols
symbolmap = {'not' : '\u00AC',
             '~' : '\u00AC',
             'equals' : '=',
             'notequals' : '\u2260',
             '=/=' : '\u2260',
             '!=' : '\u2260',
             'and' : '\u2227',
             '&' : '\u2227',
             'or' : '\u2228',
             '|' : '\u2228',
             'implies' : '\u2192',
             '->' : '\u2192',
             '==>' : '\u2192',
             'impliedby' : '\u2190',
             '<-' : '\u2190',
             '<==' : '\u2190',
             'iff' : '\u2194',
             '<->' : '\u2194',
             '<=>' : '\u2194',
             'forall' : '\u2200',
             'exists' : '\u2203'}


def get_symbol(x):
    """ Return the corresponsing logical symbol if it is in the table. Else
    return x.
    
    """
    if x in symbolmap: return symbolmap[x]
    return x


class LogicCompleter(QtWidgets.QCompleter):
    def __init__(self, parent=None):
        words = list(sorted(symbolmap.keys()))
        QtWidgets.QCompleter.__init__(self, words, parent)


class DictionaryCompleter(QtWidgets.QCompleter):
    def __init__(self, parent=None):
        words = []
        try:
            f = open("/usr/share/dict/words","r")
            for word in f:
                words.append(word.strip())
            f.close()
        except IOError:
            print("dictionary not in anticipated location")
        QtWidgets.QCompleter.__init__(self, words, parent)


class CompletionTextEdit(QTextEdit):
    """ A text editor that allows autocomplete and syntax highlighting. """
    
    # define a signal that is emited when user presses return (edit finished)
    submit = QtCore.pyqtSignal()

    def __init__(self, parent=None, delimiters=[]):
        super(CompletionTextEdit, self).__init__(parent)
        self.completer = None
        self.moveCursor(QTextCursor.End)
        self.cursorPositionChanged.connect(self._matchParentheses);
        # delimiters determine what counts as a word boundary
        # '\x00' returned when asking for a character that is out of range
        # '\u2029' - return in qtextedit (para)
        self.delimiters = ['\x00', ' ', '\t', '\n', '\u2029']
        self.delimiters.extend(delimiters)
    
    def add_delimiters(self, *delimiters):
        """ Add the given parameters to the list of delimiters. """
        self.delimiters.extend(delimiters)
    
    def del_delimiters(self, *delimiters):
        """ Remove the given parameters to the list of delimiters. """
        delims = set(delimiters)
        self.delimiters = [d for d in self.delimiters if d not in delims]

    def setCompleter(self, completer):
        if self.completer:
            self.completer.disconnect()
        if not completer:
            return

        completer.setWidget(self)
        completer.setCompletionMode(QtWidgets.QCompleter.PopupCompletion)
        completer.setCaseSensitivity(QtCore.Qt.CaseInsensitive)
        self.completer = completer
        self.completer.activated.connect(self.insertCompletion)
        
    def _matchParentheses(self):
        """ Check if the cursor is in front of '(' or behind ')' and highlight 
        the parenthsis. Use green for a matching and red for non-matching pair.
        
        """
        # first clear the current selection
        self.setExtraSelections([])
        
        # check if we are in front of an openning brace
        cursor = self.textCursor()
        cursor.movePosition(QTextCursor.NextCharacter,
                            QTextCursor.KeepAnchor)
        char = cursor.selectedText()
        if char == '(':
            level = 1
            doc = self.document()
            pos = cursor.position()
            while pos < doc.characterCount():
                char = doc.characterAt(pos)
                if char == '(':
                    level += 1
                if char == ')':
                    level -= 1
                if level == 0:
                    self._createParenthesisSelection(pos)
                    self._createParenthesisSelection(cursor.position()-1)
                    break
                pos += 1
            # no match -- highlight in red
            if level != 0:
                self._createParenthesisSelection(cursor.position()-1, 'red')

        # check if we are behind a closing brace
        cursor = self.textCursor()
        cursor.movePosition(QTextCursor.PreviousCharacter,
                            QTextCursor.KeepAnchor)
        char = cursor.selectedText()
        if char == ')':
            level = 1
            doc = self.document()
            pos = cursor.position()
            while pos > 0:
                char = doc.characterAt(pos-1)
                if char == ')':
                    level += 1
                if char == '(':
                    level -= 1
                if level == 0:
                    self._createParenthesisSelection(pos-1)
                    self._createParenthesisSelection(cursor.position())
                    break
                pos -= 1
            # no match -- highlight in red
            if level != 0:
                self._createParenthesisSelection(cursor.position(), 'red')
    
    def _createParenthesisSelection(self, pos, colour='limeGreen'):
        """ Highlight the area at a position pos (one char to the right). """
        selections = self.extraSelections()

        selection = QTextEdit.ExtraSelection()
        format = selection.format
        format.setBackground(QColor(colour))
        selection.format = format;
        
        cursor = self.textCursor()
        cursor.setPosition(pos)
        cursor.movePosition(QTextCursor.NextCharacter, QTextCursor.KeepAnchor)
        selection.cursor = cursor

        selections.append(selection)
        self.setExtraSelections(selections)

    def insertCompletion(self, completion):
        """ User selected an item from the completion list - insert it. """
        # if we are in a middle of a word, replace it
        #   otherwise just insert the completion
        tc = self.textUnderCursor()
        selected_text = tc.selectedText().strip()
        if selected_text != '' and completion.startswith(selected_text):
            # if we are inside a word, should we replace the rest?
#            tc.select(QTextCursor.WordUnderCursor)
            pass
        else:
            # prefix does not match so get the original cursor position
            tc = self.textCursor()
        symbol = get_symbol(completion)
        tc.insertText(symbol)
        self.setTextCursor(tc)

    def textUnderCursor(self):
        """ Select the text under cursor and return the textCursor instance. """
        tc = self.textCursor()
        doc = tc.document()
        pos = tc.position()
        while (doc.characterAt(pos-1) not in self.delimiters):
            tc.movePosition(QTextCursor.PreviousCharacter,
                            QTextCursor.KeepAnchor)
            pos = tc.position()
        return tc

    def focusInEvent(self, event):
        if self.completer:
            self.completer.setWidget(self);
        QTextEdit.focusInEvent(self, event)

    def keyPressEvent(self, event):
        if self.completer and self.completer.popup().isVisible():
            if event.key() in (QtCore.Qt.Key_Enter,
                               QtCore.Qt.Key_Return,
                               QtCore.Qt.Key_Escape,
                               QtCore.Qt.Key_Tab,
                               QtCore.Qt.Key_Backtab):
                event.ignore()
                return

        if self.completer and not self.completer.popup().isVisible():
            if event.key() in (QtCore.Qt.Key_Backspace,):
                QTextEdit.keyPressEvent(self, event)
                return
        
        # if a user presses enter when not in autocomplete, emit 'submit' signal
        elif self.completer and not self.completer.popup().isVisible():
            if event.key() in (QtCore.Qt.Key_Enter,
                               QtCore.Qt.Key_Return):
                self.submit.emit()
                event.ignore()
                return
        
        # if user pressed space bar, see if we can replace the last keyword
        #   with the corresponsing symbol
        if event.key() == QtCore.Qt.Key_Space:
            tc = self.textUnderCursor()
            text = tc.selectedText()
            tc.insertText(get_symbol(text) + ' ')
            self.setTextCursor(tc)
            if self.completer:
                self.completer.popup().setVisible(False)
            return

        ## has a shortcut been pressed? if so, passe it to the parent
        isShortcut = False
        if (not self.completer or not isShortcut):
            QTextEdit.keyPressEvent(self, event)

        ## ctrl or shift key on it's own??
        ctrlOrShift = event.modifiers() in (QtCore.Qt.ControlModifier ,
                QtCore.Qt.ShiftModifier)
        if ctrlOrShift and event.text() == '':
            # ctrl or shift key on it's own
            return

        if not self.completer: return

        hasModifier = ((event.modifiers() != QtCore.Qt.NoModifier) and
                        not ctrlOrShift)
                        
        completionPrefix = self.textUnderCursor().selectedText()
        #get_log().debug('completionPrefix: "{0}"'.format(completionPrefix))

        if (not isShortcut and
            (hasModifier or event.text().strip() in ['', '(', ')'])):
            self.completer.popup().hide()
            return

        if (completionPrefix != self.completer.completionPrefix()):
            self.completer.setCompletionPrefix(completionPrefix)
            popup = self.completer.popup()
            popup.setCurrentIndex(
                self.completer.completionModel().index(0,0))

        cr = self.cursorRect()
        cr.setWidth(self.completer.popup().sizeHintForColumn(0)
            + self.completer.popup().verticalScrollBar().sizeHint().width())
        self.completer.complete(cr) ## popup it up!


################################################################################


if __name__ == "__main__":

    app = QtWidgets.QApplication([])
    completer = DictionaryCompleter()
    te = CompletionTextEdit()
    te.setCompleter(completer)
    te.show()
    app.exec_()
