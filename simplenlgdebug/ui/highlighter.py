from PyQt5.QtCore import QRegExp, Qt
from PyQt5.QtGui import QColor, QFont, QSyntaxHighlighter, QTextCharFormat


# Logic symbols
symbolmap = {'not' : '\u00AC',
             '~' : '\u00AC',
             'equals' : '=',
             'notequals' : '\u2260',
             '=/=' : '\u2260',
             '!=' : '\u2260',
             'and' : '\u2227',
             '&' : '\u2227',
             'or' : '\u2228',
             '|' : '\u2228',
             'implies' : '\u2192',
             '->' : '\u2192',
             '==>' : '\u2192',
             'impliedby' : '\u2190',
             '<-' : '\u2190',
             '<==' : '\u2190',
             'iff' : '\u2194',
             '<->' : '\u2194',
             '<=>' : '\u2194',
             'forall' : '\u2200',
             'exists' : '\u2203'}


class FOLHighlighter(QSyntaxHighlighter):
    def __init__(self, parent=None):
        super(FOLHighlighter, self).__init__(parent)

        keywordFormat = format('darkGreen', 'bold')

        keywordPatterns = ["\\bforall\\b",
                           "\\bexists\\b",
                           "\\bnot\\b",
                           "\\band\\b",
                           "\\bor\\b",
                           "\\bimplies\\b",
                           "\\bimpliedby\\b",
                           "\\biff\\b",
                           "\\bequals\\b",
                           "\\bnotequals\\b"
                           ]

        self.highlightingRules = [(QRegExp(pattern,
                                           syntax=QRegExp.RegExp),
                                   keywordFormat)
                for pattern in keywordPatterns]

        keywordPatterns = ["&", "|", "->", "==>", "~", "<-", "<==",
                           "<->", "<=>", "=/=", "!=", "="]
        self.highlightingRules += [(QRegExp(pattern,
                                            syntax=QRegExp.FixedString),
                                   keywordFormat)
                for pattern in keywordPatterns]
        
        self.highlightingRules += [(QRegExp(pattern,
                                            syntax=QRegExp.FixedString),
                                   keywordFormat)
                for pattern in symbolmap.values()]


        classFormat = QTextCharFormat()
        classFormat.setFontWeight(QFont.Bold)
        classFormat.setForeground(Qt.darkMagenta)
        self.highlightingRules.append((QRegExp("\\bQ[A-Za-z]+\\b"),
                classFormat))

        singleLineCommentFormat = format('green')
        self.highlightingRules.append((QRegExp("#[^\n]*"),
                singleLineCommentFormat))

        self.multiLineCommentFormat = format('darkGreen')

        quotationFormat = format('red')
        self.highlightingRules.append((QRegExp("\".*\""), quotationFormat))

        functionFormat = format('blue', 'italic')
        self.highlightingRules.append((QRegExp("\\b[A-Za-z0-9_]+(?=\\()"),
                                       functionFormat))

        self.commentStartExpression = QRegExp("/\\*")
        self.commentEndExpression = QRegExp("\\*/")

    def highlightBlock(self, text):
        for expression, format in self.highlightingRules:
            index = expression.indexIn(text)
            while index >= 0:
                length = expression.matchedLength()
                self.setFormat(index, length, format)
                index = expression.indexIn(text, index + length)

        self.setCurrentBlockState(0)

        startIndex = 0
        if self.previousBlockState() != 1:
            startIndex = self.commentStartExpression.indexIn(text)

        while startIndex >= 0:
            endIndex = self.commentEndExpression.indexIn(text, startIndex)

            if endIndex == -1:
                self.setCurrentBlockState(1)
                commentLength = len(text) - startIndex
            else:
                commentLength = (endIndex - startIndex +
                                 self.commentEndExpression.matchedLength())

            self.setFormat(startIndex, commentLength,
                    self.multiLineCommentFormat)
            startIndex = self.commentStartExpression.indexIn(text,
                    startIndex + commentLength);


class TemplateHighlighter(QSyntaxHighlighter):
    def __init__(self, parent, keywords):
        super(TemplateHighlighter, self).__init__(parent)

        keywordFormat = format('maroon', 'bold')

        self.highlightingRules = [(QRegExp(pattern,
                                            syntax=QRegExp.FixedString),
                                   keywordFormat)
                for pattern in keywords]
        
        classFormat = QTextCharFormat()
        classFormat.setFontWeight(QFont.Bold)
        classFormat.setForeground(Qt.darkMagenta)
        self.highlightingRules.append((QRegExp("\\bQ[A-Za-z]+\\b"),
                classFormat))

        singleLineCommentFormat = format('green')
        self.highlightingRules.append((QRegExp("#[^\n]*"),
                singleLineCommentFormat))

        self.multiLineCommentFormat = format('darkGreen')

        quotationFormat = format('blue')
        self.highlightingRules.append((QRegExp("\"[^\"]*\""), quotationFormat))
        self.highlightingRules.append((QRegExp("\'[^\']*\'"), quotationFormat))

#        functionFormat = format('blue', 'italic')
#        self.highlightingRules.append((QRegExp("\\b[A-Za-z0-9_]+(?=\\()"),
#                                       functionFormat))

        self.commentStartExpression = QRegExp("/\\*")
        self.commentEndExpression = QRegExp("\\*/")

    def highlightBlock(self, text):
        for expression, format in self.highlightingRules:
            index = expression.indexIn(text)
            while index >= 0:
                length = expression.matchedLength()
                self.setFormat(index, length, format)
                index = expression.indexIn(text, index + length)

        self.setCurrentBlockState(0)

        startIndex = 0
        if self.previousBlockState() != 1:
            startIndex = self.commentStartExpression.indexIn(text)

        while startIndex >= 0:
            endIndex = self.commentEndExpression.indexIn(text, startIndex)

            if endIndex == -1:
                self.setCurrentBlockState(1)
                commentLength = len(text) - startIndex
            else:
                commentLength = (endIndex - startIndex +
                                 self.commentEndExpression.matchedLength())

            self.setFormat(startIndex, commentLength,
                    self.multiLineCommentFormat)
            startIndex = self.commentStartExpression.indexIn(text,
                    startIndex + commentLength);


class XMLHighlighter(QSyntaxHighlighter):
    def __init__(self, parent, keywords):
        super(XMLHighlighter, self).__init__(parent)

        keywordFormat = format('maroon', 'bold')

        self.highlightingRules = [(QRegExp(pattern,
                                            syntax=QRegExp.FixedString),
                                   keywordFormat)
                for pattern in keywords]
        
        tagFormat = QTextCharFormat()
        tagFormat.setFontWeight(QFont.Bold)
        tagFormat.setForeground(Qt.darkMagenta)
        self.highlightingRules.append((QRegExp("<[^<]+>"),tagFormat))

        self.multiLineCommentFormat = format('darkGreen')

        quotationFormat = format('blue')
        self.highlightingRules.append((QRegExp("\"[^\"]*\""), quotationFormat))
        self.highlightingRules.append((QRegExp("\'[^\']*\'"), quotationFormat))

        self.commentStartExpression = QRegExp("<!--")
        self.commentEndExpression = QRegExp("-->")

    def highlightBlock(self, text):
        for expression, format in self.highlightingRules:
            index = expression.indexIn(text)
            while index >= 0:
                length = expression.matchedLength()
                self.setFormat(index, length, format)
                index = expression.indexIn(text, index + length)

        self.setCurrentBlockState(0)

        startIndex = 0
        if self.previousBlockState() != 1:
            startIndex = self.commentStartExpression.indexIn(text)

        while startIndex >= 0:
            endIndex = self.commentEndExpression.indexIn(text, startIndex)

            if endIndex == -1:
                self.setCurrentBlockState(1)
                commentLength = len(text) - startIndex
            else:
                commentLength = (endIndex - startIndex +
                                 self.commentEndExpression.matchedLength())

            self.setFormat(startIndex, commentLength,
                    self.multiLineCommentFormat)
            startIndex = self.commentStartExpression.indexIn(text,
                    startIndex + commentLength);


def format(color_name, style=''):
    """ Return a QTextCharFormat with the given attributes. """
    color = QColor()
    color.setNamedColor(color_name)

    format = QTextCharFormat()
    format.setForeground(color)
    if 'bold' in style:
       format.setFontWeight(QFont.Bold)
    if 'italic' in style:
       format.setFontItalic(True)

    return format


#############################################################################
##
## Copyright (C) 2013 Riverbank Computing Limited.
## Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
## All rights reserved.
##
## This file is part of the examples of PyQt.
##
## $QT_BEGIN_LICENSE:BSD$
## You may use this file under the terms of the BSD license as follows:
##
## "Redistribution and use in source and binary forms, with or without
## modification, are permitted provided that the following conditions are
## met:
##   * Redistributions of source code must retain the above copyright
##     notice, this list of conditions and the following disclaimer.
##   * Redistributions in binary form must reproduce the above copyright
##     notice, this list of conditions and the following disclaimer in
##     the documentation and/or other materials provided with the
##     distribution.
##   * Neither the name of Nokia Corporation and its Subsidiary(-ies) nor
##     the names of its contributors may be used to endorse or promote
##     products derived from this software without specific prior written
##     permission.
##
## THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
## "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
## LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
## A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
## OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
## SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
## LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
## DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
## THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
## (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
## OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
## $QT_END_LICENSE$
##
#############################################################################