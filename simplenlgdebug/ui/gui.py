import logging


def get_log():
    return logging.getLogger(__name__)

get_log().addHandler(logging.NullHandler())


import PyQt5.QtGui as QtGui
import PyQt5.QtCore as QtCore
import PyQt5.QtWidgets as QtWidgets

from ui.mainwindow import Ui_simpleNLGTesterWindow
from ui.highlighter import TemplateHighlighter, XMLHighlighter

import nlg.nlg as nlg
from nlg.structures import (Element, String, Word, Clause, Phrase, Coordination,
                            NounPhrase, VerbPhrase, PrepositionalPhrase,
                            AdjectivePhrase, AdverbPhrase, PlaceHolder)
from nlg.microplanning import (Noun, Verb, Adjective, Adverb, Preposition,
                               Determiner, Pronoun, Conjunction, Preposition,
                               Numeral, Symbol, NP, VP, PP, AdvP, AdjP,
                               NN, NNS, NNP, NNPS,
                               Features, Case, Number, Gender, Person, Tense,
                               Aspect, Mood, Modal, Voice,
                               Form, InterrogativeType)
from nlg.microplanning import XmlVisitor



class SimpleNLGTesterWindow(QtWidgets.QMainWindow, Ui_simpleNLGTesterWindow):
    def __init__(self, parent=None, f=QtCore.Qt.WindowFlags()):
        QtWidgets.QMainWindow.__init__(self, parent, f)

        self.setupUi(self)
        tab_spaces = 2
        metrics = QtGui.QFontMetrics(self.pythonInputTextEdit.currentFont())
        tab_width = tab_spaces * metrics.width(' ')
        self.pythonInputTextEdit.setTabStopWidth(tab_width)
        self.xmlInputTextEdit.setTabStopWidth(tab_width)
        keywords = ['Element', 'String', 'PlaceHolder', 'Word',
            'Noun', 'Verb', 'Adjective', 'Adverb', 'Preposition', 'Determiner',
            'Pronoun', 'Conjunction', 'Preposition', 'Numeral', 'Symbol',
            'Clause', 'NP', 'VP', 'PP', 'AdvP', 'AdjP',
            'NN', 'NNP', 'NounPhrase', 'VerbPhrase',
            'AdjectivePhrase', 'AdverbPhrase', 'PrepositionalPhrase',
            'Coordination',
            'Features', 'Case', 'Number', 'Gender', 'Person', 'Tense',
            'Aspect', 'Mood', 'Modal', 'Voice', 'Form', 'InterrogativeType'
        ]
        self.template_highlighter = \
            TemplateHighlighter(self.pythonInputTextEdit.document(), keywords)
        self.pythonInputTextEdit.setCompleter(QtWidgets.QCompleter(keywords,
                                              self.pythonInputTextEdit))
        self.pythonInputTextEdit.add_delimiters('(')
        keywords = [
            'xmlns', 'NLGSpec', 'schemaLocation', 'Request',
            'nlg', 'Document', 'child', 'subj', 'val', 'xsi', 'xsi:type',
            'head', 'postMod', 'frontMod', 'coord', 'vp', 'base',
            'StringElement', 'WordElement', 'cat', 'SPhraseSpec',
            'NPPhraseSpec', 'VPPhraseSpec', 'PPPhraseSpec', 'AdjPhraseSpec',
            'AdvPhraseSpec', 'CoordinatedPhraseElement', 'true', 'false',
            'NEGATED', 'COMPLEMENTISER',
        ]
        self.xml_highlighter = \
            XMLHighlighter(self.xmlInputTextEdit.document(), keywords)
        self.xmlInputTextEdit.setCompleter(QtWidgets.QCompleter(keywords,
                                              self.xmlInputTextEdit))
        self.xmlInputTextEdit.add_delimiters('<', '>', ':', '/', '"', "'")
        self.processPythonButton.clicked.connect(self.handle_process_python)
        self.processXMLButton.clicked.connect(self.handle_process_xml)
        self.clearButton.clicked.connect(self.handle_clear)
        self.splitter.setSizes((70, 400, 70))
        self.pythonInputTextEdit.setFocus()

    def handle_clear(self):
        """ Clear the output. """
        self.outputTextEdit.setText('')

    def handle_process_python(self):
        code = self.pythonInputTextEdit.toPlainText().strip()
        msg = ''
        try:
            t = eval(code)
            v = XmlVisitor()
            t.accept(v)
            xml = v.to_xml()
            self.xmlInputTextEdit.setText(xml)
            msg = nlg.simplenlg_client.xml_request(xml)
        except Exception as e:
            msg = str(e)
        self.outputTextEdit.append(msg + '\n')

    def handle_process_xml(self):
        xml = self.xmlInputTextEdit.toPlainText()
        msg = ''
        try:
            msg = nlg.simplenlg_client.xml_request(xml)
        except Exception as e:
            msg = str(e)
        self.outputTextEdit.append(msg)
        self.outputTextEdit.ensureCursorVisible()



#############################################################################
##
## Copyright (C) 2014 Roman Kutlak, University of Aberdeen.
## All rights reserved.
##
## This file is part of SAsSy Logic program.
##
## You may use this file under the terms of the BSD license as follows:
##
## "Redistribution and use in source and binary forms, with or without
## modification, are permitted provided that the following conditions are
## met:
##   * Redistributions of source code must retain the above copyright
##     notice, this list of conditions and the following disclaimer.
##   * Redistributions in binary form must reproduce the above copyright
##     notice, this list of conditions and the following disclaimer in
##     the documentation and/or other materials provided with the
##     distribution.
##   * Neither the name of University of Aberdeen nor
##     the names of its contributors may be used to endorse or promote
##     products derived from this software without specific prior written
##     permission.
##
## THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
## "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
## LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
## A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
## OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
## SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
## LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
## DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
## THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
## (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
## OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
##
#############################################################################