# Licence at the bottom.
# Code from:
#   http://victorlin.me/posts/2012/08/26/good-logging-practice-in-python
# format fields:
#   http://docs.python.org/3.3/library/logging.html#logrecord-attributes

import os
import sys
import sip
import utils
import logging


sys.path.append('sassynlg')

import nlg.nlg


# run the graphical user interface
def run_gui():
    from PyQt5.QtWidgets import QApplication
    from ui.gui import SimpleNLGTesterWindow

    app = QApplication(sys.argv)
    window = SimpleNLGTesterWindow()
    window.show()
    return app.exec_()


if __name__ == "__main__":
    try:
        print('Setting up logging...')
        utils.try_setup_logging()
        utils.get_log().debug('logging set up')
    except Exception as e:
        print('Exception while setting up logging:' + str(e))
    try:
        if hasattr(sys, "frozen"):
            nlg.nlg.init('application.settings')
        else:
            nlg.nlg.init_from_settings(
                'sassynlg/nlg/resources/simplenlg.settings')
    except Exception as e:
        utils.get_log().exception(e)
    res = 0
    try:
        res = run_gui()
    except Exception as e:
        utils.get_log().exception(e)
    try:
        nlg.nlg.shutdown()
    except Exception as e:
        utils.get_log().exception(e)
    sys.exit(res)


#############################################################################
##
## Copyright (C) 2014 Roman Kutlak, University of Aberdeen.
## All rights reserved.
##
## This file is part of SAsSy Logic Program.
##
## You may use this file under the terms of the BSD license as follows:
##
## "Redistribution and use in source and binary forms, with or without
## modification, are permitted provided that the following conditions are
## met:
##   * Redistributions of source code must retain the above copyright
##     notice, this list of conditions and the following disclaimer.
##   * Redistributions in binary form must reproduce the above copyright
##     notice, this list of conditions and the following disclaimer in
##     the documentation and/or other materials provided with the
##     distribution.
##   * Neither the name of University of Aberdeen nor
##     the names of its contributors may be used to endorse or promote
##     products derived from this software without specific prior written
##     permission.
##
## THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
## "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
## LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
## A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
## OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
## SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
## LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
## DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
## THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
## (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
## OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
##
#############################################################################