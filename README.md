A utility for debugging "conversation" between Python/XML and java SimpleNLG server.

The program runs its own SimpleNLG server from a jar file inside the bundle. By default, the server will use port 50007 and the client will use ths same port on localhost. These values can be changed in the settings file (application.settings inside the app bundle).
