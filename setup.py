"""
Usage:
    python3 setup.py py2app
"""

from setuptools import setup, find_packages
from setuptools.command.test import test as TestCommand
import io
import codecs
import os
import sys


APP = ['simplenlgdebug/main.py']
DATA_FILES = []
OPTIONS = {
            'argv_emulation': True
          }

here = os.path.abspath(os.path.dirname(__file__))

def read(*filenames, **kwargs):
    encoding = kwargs.get('encoding', 'utf-8')
    sep = kwargs.get('sep', '\n')
    buf = []
    for filename in filenames:
        with io.open(filename, encoding=encoding) as f:
            buf.append(f.read())
    return sep.join(buf)

#long_description = read('README.txt', 'CHANGES.txt')
long_description = """
A debugging tool that allows sending either python NLG data structures 
or XML to a SimpleNLG server for realisation.

"""


class PyTest(TestCommand):
    def finalize_options(self):
        TestCommand.finalize_options(self)
        self.test_args = []
        self.test_suite = True

    def run_tests(self):
        import pytest
        errcode = pytest.main(self.test_args)
        sys.exit(errcode)

setup(
    name='SimpleNlgDebug',
    version=0.1,
    url='http://bitbucket.org/rkutlak/simplenlgdebug',
    license='BSD',
    app=APP,
    data_files=DATA_FILES,
    options={'py2app': OPTIONS},
    author='Roman Kutlak',
    install_requires=['rdflib>=3.0',
                      'pyyaml>=1.0',
                     ],
    tests_require=['pytest'],
    setup_requires=['py2app'],
    extras_require={
        'testing': ['pytest'],
    },
    cmdclass={'test': PyTest},
    author_email='roman@kutlak.net',
    description='Simple NLG Testing and Debugging App.',
    long_description=long_description,
    packages=['simplenlgdebug'],
    include_package_data=True,
    platforms='any',
    test_suite='simplenlgdebug.test.main',
    classifiers = [
        'Programming Language :: Python',
        'Development Status :: 1 - Alpha',
        'Natural Language :: English',
        'Intended Audience :: Developers',
        'License :: BSD',
        'Operating System :: OS Independent',
        'Topic :: Software Development :: Libraries :: Application Frameworks',
#        'Environment :: Web Environment',
#        'Topic :: Software Development :: Libraries :: Python Modules',
#        'Topic :: Internet :: WWW/HTTP :: Dynamic Content',
        ]
)